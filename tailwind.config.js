module.exports = {
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
      },
      colors: {
        gray: {
          '100': 'var(--color-gray-100)',
          '200': 'var(--color-gray-200)',
        },
        red: {
          '100': 'var(--color-red-100)',
        },
        green: {
          '100': 'var(--color-green-100)',
        },
        blue: {
          '100': 'var(--color-blue-100)',
          '200': 'var(--color-blue-200)',
        },
      },
    },
  },
  variants: {},
  plugins: [],
};
