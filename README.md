## Hello

The task was solved with the help of the following stack:

- ReactJs (create-react-app)
- TypeScript
- Tailwind
- React Testing Library

Its deployed on [netlify](https://livechat-login-task.netlify.app/) and the code is available online as well on [GitLab](https://gitlab.com/bartosz.gamza/livechat-login)

### Run development server:

```bash
yarn install
yarn start
```
### Tests
```bash
yarn test
```

All of requirements are fullfilled I believe. Maybe it would need a tweak to run on IE properly but its up to discussion. I've added specs as requested, focued mostly on Login component. Perhaps they could be more preceise. There is a warning poping up when running them - its caused by timeout interval for fake API request I made - it would not occur in normal environment especially with using library like ReactAsync for example. I did not focus on styling too much, I stole the colours and logo from your site deliberately 🌚.

Enjoy reviewing, feel free to contact me via bartosz.gamza12@gmail.com

Cheers,

Bartosz 🙌
