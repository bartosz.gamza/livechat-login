import React from 'react';
import Header from 'components/Header';
import Login from 'views/Login';

function App() {
  return (
    <>
      <Header />
      <div className="view w-full bg-gray-100 pt-6">
        <div className="container mx-auto flex justify-center">
          <Login />
        </div>
      </div>
    </>
  );
}

export default App;
