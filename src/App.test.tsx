import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

describe('App', () => {
  it('renders nested components', async () => {
    const { findByTestId } = render(<App />);
    expect(await findByTestId('header')).toBeInTheDocument();
    expect(await findByTestId('login')).toBeInTheDocument();
  });
});
