import React from 'react';
import { render } from '@testing-library/react';
import InfoLabel from './InfoLabel';

describe('InfoLabel', () => {
  it('renders correct message and theme', async () => {
    const { findByText } = render(<InfoLabel info="Failure" success={false} />);
    const label = await findByText('Failure');
    expect(label).toBeInTheDocument();
    expect(label).toHaveClass('bg-red-100');
  });
});
