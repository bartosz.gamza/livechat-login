import React, { FC } from 'react';

type Props = {
  info: string;
  success?: boolean;
};

const InfoLabel: FC<Props> = ({ info, success = true }: Props) => {
  return (
    <div
      data-testid="info-label"
      className={`flex justify-center m-4 font-poppins p-6 sm:p-8 shadow-lg rounded text-white ${
        success ? 'bg-green-100' : 'bg-red-100'
      }`}
    >
      {info}
    </div>
  );
};

export default InfoLabel;
