import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import Input from './Input';

describe('Input', () => {
  afterEach(() => cleanup);
  const baseProps = {
    id: 'test',
    type: 'password',
    name: 'test-name',
    value: 'P@ssw0rd',
    placeholder: 'Password',
    onChange: jest.fn(),
  };
  describe('base state', () => {
    it('uses props propetrly', async () => {
      const { findByTestId } = render(<Input {...baseProps} />);
      const input = await findByTestId('text-input');
      expect(input).toBeInTheDocument();
      expect(input).toHaveProperty('id', baseProps.id);
      expect(input).toHaveProperty('type', baseProps.type);
      expect(input).toHaveProperty('name', baseProps.name);
      expect(input).toHaveProperty('placeholder', baseProps.placeholder);
      expect(input).toHaveProperty('value', baseProps.value);
    });
  });
  describe('when there is a label prop defined', () => {
    it('renders the label correctly', async () => {
      const { findByLabelText } = render(<Input {...baseProps} label="test-label" />);
      expect(await findByLabelText('test-label')).toBeInTheDocument();
    });
  });
  describe('when there is an error defined', () => {
    it('renders the message correctly', async () => {
      const { findByText } = render(<Input {...baseProps} errors="test-error" />);
      expect(await findByText('test-error')).toBeInTheDocument();
    });
    it('it changes the border color', async () => {
      const { findByTestId } = render(<Input {...baseProps} errors="test-error" />);
      expect(await findByTestId('text-input')).toHaveClass('border-red-100');
    });
  });
  describe('changing value', () => {
    beforeEach(async () => {
      const { findByTestId } = render(<Input {...baseProps} />);
      const input = await findByTestId('text-input');
      fireEvent.change(input, { target: { value: 'test value' } });
    });
    it('emits change', () => {
      expect(baseProps.onChange).toHaveBeenCalled();
    });
  });
});
