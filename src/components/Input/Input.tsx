import React, { FC, ChangeEvent } from 'react';

type Props = {
  id: string;
  type?: string;
  value: string;
  name: string;
  placeholder: string;
  label?: string;
  errors?: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
};

const Input: FC<Props> = ({ id, type = 'text', value, name, placeholder, label, errors, onChange }: Props) => {
  return (
    <>
      {label && (
        <label className="mb-2" htmlFor={id}>
          {label}
        </label>
      )}
      <input
        data-testid="text-input"
        className={`outline-none rounded border ${
          errors?.length ? 'border-red-100' : 'border-black focus:border-blue-100'
        } h-10 px-3 w-full`}
        type={type}
        id={id}
        value={value}
        name={name}
        placeholder={placeholder}
        onChange={onChange}
      />
      <p className="h-6 py-1 text-red-100 text-sm font-light">{errors}</p>
    </>
  );
};

export default Input;
