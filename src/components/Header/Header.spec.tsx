import React from 'react';
import { render } from '@testing-library/react';
import Header from './Header';

describe('header', () => {
  it('renders logo', async () => {
    const { findByTestId } = render(<Header />);
    expect(await findByTestId('livechat-logo')).toBeInTheDocument();
  });
});
