import React, { FC } from 'react';
import logo from 'assets/images/livechat.svg';

const Header: FC = () => {
  return (
    <header data-testid="header" className="flex justify-center w-full h-16 border-gray-200 border-b">
      <img data-testid="livechat-logo" src={logo} alt="LiveChat" />
    </header>
  );
};

export default Header;
