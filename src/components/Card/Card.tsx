import React, { FC, ReactNode } from 'react';

type Props = {
  title?: string;
  children: ReactNode;
};

const Card: FC<Props> = ({ title = '', children }: Props) => {
  return (
    <div data-testid="card" className="flex flex-col w-full p-4 font-poppins">
      <h1 className="font-bold text-3xl mb-4">{title}</h1>
      <div className="p-6 sm:p-8 shadow-lg rounded bg-white">{children}</div>
    </div>
  );
};

export default Card;
