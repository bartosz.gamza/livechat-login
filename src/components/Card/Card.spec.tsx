import React from 'react';
import { render } from '@testing-library/react';
import Card from './Card';

describe('Card', () => {
  it('renders title and children', async () => {
    const { findByText } = render(
      <Card title="cardTitle">
        <div>Child</div>
      </Card>,
    );
    expect(await findByText(/cardTitle/)).toBeInTheDocument();
    expect(await findByText(/Child/)).toBeInTheDocument();
  });
});
