import React, { FC, useState, ChangeEvent, FormEvent, useEffect } from 'react';

import Card from 'components/Card';
import Input from 'components/Input';
import InfoLabel from 'components/InfoLabel';

import { login, validateForm, validateField } from 'services/auth';
import { LoginFormType, LoginErrorsType } from 'services/auth/types';

const initialFormState: LoginFormType = { email: '', password: '', remember: false };
const initialErrorsState: LoginErrorsType = { email: '', password: '' };
const initialSubmitingState = false;
const initialAuthorisationState = '';

const LoginView: FC = () => {
  const [form, setForm] = useState(initialFormState);
  const [errors, setErrors] = useState(initialErrorsState);
  const [isSubmitting, setSubmitting] = useState(initialSubmitingState);
  const [authorisationState, setAuthorisationState] = useState(initialAuthorisationState);

  useEffect(() => {
    if (isSubmitting) {
      const authorise = async () => {
        await login(form).then((resp) => {
          const authState = resp.data.message;
          setAuthorisationState(authState);
          setSubmitting(false);
        });
      };
      authorise();
    }
  }, [isSubmitting, form]);

  const inputHandler = (e: ChangeEvent<HTMLInputElement>): void => {
    e.persist();
    if (errors[e.target.name]) {
      setErrors({
        ...errors,
        [e.target.name]: validateField(e.target.name, e.target.value),
      });
    }
    setForm({
      ...form,
      [e.target.name]: e.target.type === 'checkbox' ? e.target.checked : e.target.value,
    });
  };

  const submitHandler = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    const { email, password } = validateForm(form);
    if (Object.values({ email, password }).some((err) => err.length)) {
      setErrors({
        ...errors,
        email,
        password,
      });
    } else {
      setSubmitting(true);
    }
  };

  return (
    <section data-testid="login" className="w-full md:w-1/2 lg:w-1/3">
      {authorisationState === 'success' ? (
        <InfoLabel info="Success! ✓" />
      ) : (
        <Card title="Login">
          <form onSubmit={submitHandler}>
            <fieldset className="flex flex-col">
              <Input
                id="email"
                name="email"
                label="Email"
                placeholder="test@test.pl"
                value={form.email}
                errors={errors.email || ''}
                onChange={inputHandler}
              />
              <Input
                id="password"
                name="password"
                label="Password"
                type="password"
                placeholder="Password1"
                value={form.password}
                errors={errors.password || ''}
                onChange={inputHandler}
              />
              <label className="mb-4 cursor-pointer">
                <input
                  data-testid="remember-checkbox"
                  type="checkbox"
                  checked={form.remember}
                  onChange={inputHandler}
                  name="remember"
                  className="leading-tight"
                />
                <span className="ml-3">Remember me</span>
              </label>
              <input
                data-testid="submit-button"
                className="outline-none h-10 px-3 bg-blue-200 hover:bg-blue-100 text-white rounded w-full cursor-pointer"
                disabled={isSubmitting}
                type="submit"
                value={isSubmitting ? 'Loading' : 'Login'}
              />
            </fieldset>
          </form>
        </Card>
      )}
      {authorisationState === 'error' && <InfoLabel info="Invalid email or password." success={false} />}
    </section>
  );
};

export default LoginView;
