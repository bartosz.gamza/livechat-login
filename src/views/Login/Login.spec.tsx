import React from 'react';
import { render, cleanup, RenderResult, fireEvent } from '@testing-library/react';
import Login from './Login';

describe('login', () => {
  afterEach(() => cleanup);
  jest.useFakeTimers();
  let utils: RenderResult;
  beforeEach(() => {
    utils = render(<Login />);
  });
  async function submitting() {
    fireEvent.click(await utils.findByTestId('submit-button'));
  }
  function providingInput(email: string, password: string) {
    const emailInput = utils.getByLabelText('Email');
    const passwordInput = utils.getByLabelText('Password');
    fireEvent.change(emailInput, { target: { value: email } });
    fireEvent.change(passwordInput, { target: { value: password } });
  }
  it('renders all the nested components correctly', async () => {
    expect(await utils.findByTestId('card')).toBeInTheDocument();
    expect(await utils.findAllByTestId('text-input')).toHaveLength(2);
    expect(await utils.findByTestId('remember-checkbox')).toBeInTheDocument();
    expect(await utils.findByTestId('submit-button')).toBeInTheDocument();
  });
  describe('initial state', () => {
    it('does not render any InfoLabel components', () => {
      expect(utils.queryByText(/Success/)).not.toBeInTheDocument();
      expect(utils.queryByText(/Invalid email or password./)).not.toBeInTheDocument();
    });

    describe('submitting form without providing any data', () => {
      beforeEach(async () => {
        await submitting();
      });
      it('renders error messages under fields', () => {
        expect(utils.queryAllByText(/This field is required./)).toHaveLength(2);
      });
    });
  });
  describe('providing wrong data syntax', () => {
    beforeEach(() => {
      providingInput('some', 'data');
    });
    it('does not validate just yet', () => {
      expect(utils.queryByText(/Invalid email syntax/)).not.toBeInTheDocument();
      expect(utils.queryByText(/Invalid password syntax/)).not.toBeInTheDocument();
    });
    describe('submitting the form', () => {
      beforeEach(async () => await submitting());
      it('triggers validation and displays errors accordingly', () => {
        expect(utils.queryByText(/Invalid email syntax/)).toBeInTheDocument();
        expect(utils.queryByText(/Invalid password syntax/)).toBeInTheDocument();
      });
    });
  });
  describe('providing correct data syntax but wrong credentials', () => {
    beforeEach(async () => {
      providingInput('test@test.com', 'Password1');
      await submitting();
    });
    it('submits the form', () => {
      expect(utils.queryByText(/Loading/)).toBeInTheDocument();
    });
    it('renders the InfoLabel with authorisation error', async () => {
      jest.runAllTimers();
      expect(await utils.findByText(/Invalid email or password/)).toBeInTheDocument();
    });
  });
  describe('providing correct data syntax and correct credentials', () => {
    beforeEach(async () => {
      providingInput('test@test.pl', 'Password1');
      await submitting();
    });
    it('submits the form', () => {
      expect(utils.queryByText(/Loading/)).toBeInTheDocument();
    });
    it('renders the InfoLabel with authorisation success and hides the form', async () => {
      jest.runAllTimers();
      expect(await utils.findByText(/Success/)).toBeInTheDocument();
      expect(utils.queryByText(/Login/)).not.toBeInTheDocument();
    });
  });
});
