import { AUTHORIZED_EMAIL, AUTHORIZED_PASSWORD, EMAIL_REGEX, PASSWORD_REGEX } from './consts';
import * as authTypes from './types';

const validationRules: authTypes.ValidationRules = {
  email: {
    required: true,
    regex: EMAIL_REGEX,
    message: 'Invalid email syntax.',
  },
  password: {
    required: true,
    regex: PASSWORD_REGEX,
    message: 'Invalid password syntax.',
  },
};

export function validateForm(form: authTypes.LoginFormType): authTypes.LoginErrorsType {
  const errors: authTypes.LoginErrorsType = {
    email: '',
    password: '',
  };
  Object.keys(form).forEach((field) => {
    errors[field] = validateField(field, String(form[field]));
  });
  return errors;
}

export function validateField(field: keyof authTypes.LoginFormType, value: string): string {
  let error = '';
  const rule: authTypes.ValidationRule = validationRules[field];
  if (rule) {
    if (rule.required && !value) {
      error = 'This field is required.';
    } else if (!rule.regex.test(value)) {
      error = rule.message;
    }
  }
  return error;
}

export async function login(form: authTypes.LoginFormType): Promise<authTypes.FakeResponse> {
  return new Promise((resolve) => setTimeout(resolve, 1000, resolveFakeResponse(form)));
}

const resolveFakeResponse = (form: authTypes.LoginFormType): authTypes.FakeResponse => {
  const valid: boolean = validateCredentials(form);
  return {
    code: valid ? 200 : 401,
    data: { message: valid ? 'success' : 'error' },
  };
};

const validateCredentials = (form: authTypes.LoginFormType): boolean => {
  return form.email === AUTHORIZED_EMAIL && form.password === AUTHORIZED_PASSWORD;
};
