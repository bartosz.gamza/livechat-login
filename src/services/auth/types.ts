export type LoginFormType = {
  [key: string]: string | boolean;
  email: string;
  password: string;
  remember: boolean;
};

export type LoginErrorsType = {
  [key: string]: string;
  email: string;
  password: string;
};

export type ValidationRules = {
  [key: string]: ValidationRule;
};

export type ValidationRule = {
  required: boolean;
  regex: RegExp;
  message: string;
};

export type FakeResponse = {
  code: number;
  data: FakeResponseData;
};

export type FakeResponseData = {
  message: string;
};
